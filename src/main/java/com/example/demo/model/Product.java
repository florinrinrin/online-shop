package com.example.demo.model;

import com.example.demo.model.enums.Category;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
public class Product {
    @Id
    @GeneratedValue
    private Integer productId;

    private String name;
    private Integer price;
    private String description;
    private Integer quantity;

    @Enumerated(value = EnumType.STRING)
    private Category category;

    @ManyToMany
    @JoinTable(joinColumns = @JoinColumn(name = "productId"), inverseJoinColumns = @JoinColumn(name = "wishlistId"))
    private List<Wishlist> wishlists;

    @ManyToMany
    @JoinTable(joinColumns = @JoinColumn(name = "productId"), inverseJoinColumns = @JoinColumn(name = "shoppingCartId"))
    private List<ShoppingCart> shoppingCarts;

    @ManyToMany(mappedBy = "products")
    private List<Order> orders;

    @Lob //field-ul care va stoca byte cu byte imaginea
    private byte[] image;
}
