package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class Wishlist {

    @Id
    @GeneratedValue
    private Integer WishlistId;
    @OneToOne
    private User user;
    @ManyToMany(mappedBy = "wishlists")
    private List<Product> productList;
}
