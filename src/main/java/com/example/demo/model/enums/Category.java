package com.example.demo.model.enums;

public enum Category {
    ELECTRONICS, TOYS, FURNITURE, BOOKS;
}
