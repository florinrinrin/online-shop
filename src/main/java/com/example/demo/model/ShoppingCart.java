package com.example.demo.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
public class ShoppingCart {
    @Id
    @GeneratedValue
    private Integer shoppingCartId;

    @OneToOne
    private User user;

    @ManyToMany(mappedBy = "shoppingCarts")
    private List<Product> products;
}
