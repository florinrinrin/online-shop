package com.example.demo.service;

import com.example.demo.dto.ProductDto;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.model.Product;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
<<<<<<< HEAD

@Service
public class ProductService {
    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductRepository productRepository;

    public void addProduct(ProductDto productDto){
    Product product = productMapper.mapProduct(productDto);
    productRepository.save(product);
=======
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductMapper productMapper;

    public void addProduct(ProductDto productDto,MultipartFile multipartFile) {
        Product product = productMapper.map(productDto,multipartFile);
        productRepository.save(product);
    }

    public List<ProductDto> getAllAvailableProductDtos() {
        List<Product> productList = productRepository.findAll();
        List<ProductDto> result = new ArrayList<>();
        productList.forEach(product -> result.add(productMapper.map(product)));
        return result;
>>>>>>> feature2106
    }
}
