package com.example.demo.controllers;

import com.example.demo.dto.ProductDto;
import com.example.demo.service.ProductService;
import com.example.demo.validate.ProductDtoValidator;
import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductDtoValidator productDtoValidator;

    @GetMapping("/addProduct")
    public String addProductGet(Model model, @ModelAttribute("addProductMessage") String addProductMessage) {
        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto", productDto);
        return "addProduct";
    }

    @PostMapping("/addProduct")
    public String addProductPost(@ModelAttribute(name = "productDto") ProductDto productDto, BindingResult bindingResult, RedirectAttributes redirectAttributes,@RequestParam("image") MultipartFile multipartFile) {
        productDtoValidator.validate(productDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addProduct";
        }
        productService.addProduct(productDto,multipartFile);
        redirectAttributes.addFlashAttribute("addProductMessage", "The product was successfully added");
        return "redirect:/addProduct";
    }

    @GetMapping("/home")
    public String homeGet(Model model){
        List<ProductDto> productDtoList = productService.getAllAvailableProductDtos();
        model.addAttribute("productDtoList", productDtoList);
        System.out.println(productDtoList);
        return "home";
    }

    @GetMapping("/viewProduct")
    public String viewProductGet(){
        return "viewProduct";
    }
}
