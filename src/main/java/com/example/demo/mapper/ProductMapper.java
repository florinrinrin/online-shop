package com.example.demo.mapper;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.Product;
import com.example.demo.model.enums.Category;
<<<<<<< HEAD
import org.springframework.stereotype.Component;
=======
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
>>>>>>> feature2106

@Component
public class ProductMapper {

<<<<<<< HEAD
    public Product mapProduct (ProductDto productDto){
        Product product = new Product();
        product.setName(productDto.getProductName());
        product.setDescription(productDto.getDescription());
        product.setCategory(Category.valueOf(productDto.getCategory()));
        product.setPrice(Integer.valueOf(productDto.getPrice()));
        product.setQuantity(Integer.valueOf(productDto.getQuantity()));

        return product;
    }
=======
    public Product map(ProductDto productDto, MultipartFile multipartFile){
        Product product = new Product();
        product.setName(productDto.getName());
        product.setPrice(Integer.parseInt(productDto.getPrice()));
        product.setCategory(Category.valueOf(productDto.getCategory()));
        product.setDescription(productDto.getDescription());
        product.setQuantity(Integer.parseInt(productDto.getQuantity()));
        product.setImage(getBytes(multipartFile));
        return product;
    }

    private byte[] getBytes(MultipartFile multipartFile) {
        try {
            return multipartFile.getBytes();
        } catch (IOException e) {
            return new byte[0];
        }
    }


    public ProductDto map(Product product){
        ProductDto productDto = new ProductDto();
        productDto.setName(product.getName());
        productDto.setPrice(String.valueOf(product.getPrice()));
        productDto.setCategory(String.valueOf(product.getCategory()));
        productDto.setQuantity(String.valueOf(product.getQuantity()));
        productDto.setDescription(product.getDescription());
        productDto.setImage(Base64.encodeBase64String(product.getImage()));  // Base64 encoding - browser can translate byte to img.
        return productDto;
    }
>>>>>>> feature2106
}
