package com.example.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.awt.*;

@Getter
@Setter
@ToString
public class ProductDto {
    private String name;
    private String price;
    private String description;
    private String category;
    private String quantity;
    private String image;
}
