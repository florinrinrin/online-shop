package com.example.demo.validator;

import com.example.demo.dto.ProductDto;
import com.example.demo.model.enums.Category;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@Component
public class ProductDtoValidator {

    public void validate(ProductDto productDto, BindingResult bindingResult){

        validateProductName(productDto, bindingResult);

        validatePrice(productDto, bindingResult);

        validateCategory(productDto,bindingResult);

    }

    private void validatePrice(ProductDto productDto, BindingResult bindingResult) {
        try {
            Integer price = Integer.valueOf(productDto.getPrice());
            if (price <=0) {
                FieldError fieldError = new FieldError("productDTO", "price","Price should be a positive numerical value");
                bindingResult.addError(fieldError);
            }
        }
        catch (NumberFormatException exception){
            FieldError fieldError = new FieldError("productDto", "price","Price should be a numerical value");
            bindingResult.addError(fieldError);
        }
    }

    private void validateProductName(ProductDto productDto, BindingResult bindingResult) {
        if (productDto.getProductName().length() == 0) {
            FieldError fieldError = new FieldError("productDto", "productName","Please input a valid name");
            bindingResult.addError(fieldError);

        }
    }

    private void validateCategory(ProductDto productDto, BindingResult bindingResult) {
        try {
            Category category = Category.valueOf(productDto.getCategory());
        }
        catch (IllegalArgumentException exception){
            FieldError fieldError = new FieldError("productDto", "category","Category is not valid!");
            bindingResult.addError(fieldError);
        }
    }

    //create validate qTy
}
