package com.example.demo.controller;

import com.example.demo.dto.ProductDto;
import com.example.demo.service.ProductService;
import com.example.demo.validator.ProductDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class MainController {

    @Autowired
    private ProductService productService;
    @Autowired
    private ProductDtoValidator productDtoValidator;

    @GetMapping ("/addProduct")
    public String productPageGet(Model model, @ModelAttribute("addProductMessage") String addProductMessage){
        ProductDto productDto = new ProductDto();
        model.addAttribute("productDto",productDto);
        model.addAttribute("addProductMessage", addProductMessage);
        return "addProduct";
    }

    @PostMapping ("/addProduct")
    public String productPagePost(@ModelAttribute(name="productDto") ProductDto productDto, BindingResult bindingResult, RedirectAttributes redirectAttributes){
        productDtoValidator.validate(productDto,bindingResult);
        if (bindingResult.hasErrors()){
            return "addProduct";
        }
        productService.addProduct(productDto);
        redirectAttributes.addFlashAttribute("addProductMessage","Your product has been successfully added.");
        return "redirect:/addProduct";
    }
}
